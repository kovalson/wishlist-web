module.exports = {
    tabWidth: 4,
    printWidth: 120,
    endOfLine: "auto",
};
